from flask import Flask, request, redirect, session
from twilio.rest import TwilioRestClient
import twilio.twiml

# The session object makes use of a secret key.
SECRET_KEY = 'ACc37b5a83fea675d04597ed78efea4f85'
auth_token = "ab70c293577badcfdcf6976aedb4e866"
app = Flask(__name__)
app.config.from_object(__name__)
client = TwilioRestClient(SECRET_KEY, auth_token)

@app.route("/alert", methods=['GET', 'POST'])
def send_message():
    campaign = "Harry's Laundrolube"
    budget_type = "daily"
    budget = "500"
    balance = "1000"

    message = client.messages.create(to="+19785958482", from_="+19718034389",                                     body="Your campaign '{0} has reached its {1} budget of ${2}.  To increase the daily budget by $50, please reply 'increase 50', or choose any other amount. Your current account balance is ${3}.".format(campaign, budget_type, budget, balance))

@app.route("/", methods=['GET', 'POST'])
def respond():
    advertiser = request.values.get('From')
    body = request.values.get('Body').lower()

    if "increase" in body:
        amount = body.split()[1]
        message = increaseBudget(amount, advertiser)
    elif "check" in body:
        message = checkAccount(advertiser)

    print 4
    resp = twilio.twiml.Response()
    resp.sms(message)
    return str(resp)

def increaseBudget(amt, advertiser):
    #Post amt, advertiser to endpoint to increase budget
    #get back balance
    balance = 900;
    print 3
    #if successful:
    return "Your budget has been increased by {0}. Your new account balance is {1}".format(amt, balance)
    #if not successful:
    return "Whoops! Something went wrong, please try again later."

def checkAccount(advertiser):
    #Post to endpoint to check balance
    return "Your current account balance is {0}".format(balance)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
